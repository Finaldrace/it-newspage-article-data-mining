**Mine the article data from the it-newspage golem **

---
## How to get started?
1. Get all links from the golem archiv 
(ScrapeFromGolem())
Attention: the method online took articles from the year 1998 to 2019
2. Mine Artikel HTMl Data to Disk
Attentions: if the Script has a problem midrun, then adjust this values in the ScrapeFromGolem function and restart with the missing articles

3. Load the HTML data from Disk to Cache

4. Extract the data from the articles to DataObjects

5. Save the variables of the DataObjects to an excel file

## What are which file for?

MineFunctions.py - contains all classes and methods needed

DataMining.py - shows which methods should be called in which order

---

