# -*- coding: utf-8 -*-
"""
Created on Sat Jan  4 14:29:55 2020

@author: Rocketkiwi

this file instructs how to use the "MineFunctions.py", but there are some tricky cases where you maybe have to change some functions.

Check all paths and dependencies.
maybe you have to install some librarys.

"""

import MineFunctions

#  1. Load all Artikel List Data

ArticleLinkList = MineFunctions.ScrapeFromGolem()

counter = 1

#  2. Mine Artikel HTMl Data to Disk

# Nothing visual in the console, but you can track the 
MineFunctions.WriteArtikelListToDisk(ArticleLinkList)

#  Attentions: if the Script has a problem midrun, then adjust this values in the ScrapeFromGolem function and restart with the missing articles
#   Example:
#  century = 2007#1998
#   year = 7 #98
#   month = 5 #1
#  ArticleCount = 50392 #0  (The amount of Articles you already loaded + 1 )
#
#   Typical Error Message:
#       File "I:\Users\xxxxxx\Anaconda3\lib\site-packages\requests\adapters.py", line 498, in send
#       raise ConnectionError(err, request=request)
#       ConnectionError: ('Connection aborted.', OSError("(10054, 'WSAECONNRESET')"))
#
#  feel free to write this part more robust, if you don't want to restart it several times


------------------
# Attention Zip the Files
-----------------
# you have to zip all files before you can do Step 3.2
# Download a Zip tool like 7-zip (https://www.7-zip.org/)
# save all files to an archiv without an compression
# store this zip with the name "AllArticles.zip" in the "Golem_Articles" folder
# this step can take some hours (it depends on your machine performance)


# 3. Load the HTML Data from Disk to Cache

#Change to Path of the Script to the repo directory
PathOfScript = "I:\Repositorys\GolemDataMining\it-newspage-article-data-mining"
PathOfArticles = PathOfScript + "\\Golem_Articles"
os.chdir(PathOfScript)


# 3.1  List all Files from the Directory
ArticleList = GetArticleLinkListFromDisk(PathOfScript)
# 3.2 Sort this List naturally
naturalSorted = natural_sort(ArticleList)
# 3.3 Read Article with LinkList from Zip
ArticlesHTML = ReadFromZip(naturalSorted,PathOfArticles) 

# Alternativ if you don't have a ZIP File. but Attention: it is very very slow
HTMLData = LoadDataFromLinkList(naturalSorted,PathOfArticles)



# 4. Extract the data from the articles to DataObjects ------------------------------------------------------
ArticleDataList = CreateArticleDataObjectsFromHTML_List(ArticlesHTML)

# 5 Save the variables of the DataObjects to an excel File ("ArticleData.xlsx" in script folder)
WriteArticleDataListToExcelSheet(ArticleDataList)
