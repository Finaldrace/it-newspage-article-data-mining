# -*- coding: utf-8 -*-
"""
Created on Sat Jan  4 14:29:55 2020

@author: Rocketkiwi
"""

from bs4 import BeautifulSoup #https://www.crummy.com/software/BeautifulSoup/bs4/doc/
import requests
import os
import re # Regular Expressions
from openpyxl import Workbook # Excel
import zipfile
from datetime import datetime
#---Classes-------------------------------------------------------------------------
class ArticleData:
    PublishDate = ""
    ModifyDate = ""
    Headline = ""
    Keywords = ""
    Author =  ""
    Description = ""
    Text = ""
    Overline = ""
    
    def ShowAttributes(self):
        print(" Date: " + self.PublishDate)
        
class LinkData:
    Month = ""
    Year = ""
    MonthID = ""
    GeneralID = ""
    URL = ""
    
    def __init__(self,Month, Year, MonthID, GeneralID, URL):
        self.Month = Month
        self.Year = Year
        self.MonthID = MonthID
        self.GeneralID = GeneralID
        self.URL = URL
        

    
    def ShowAttributes(self):
        print(str(self.Year) + "_" +  str(self.Month) + "_" + str(self.MonthID) + "_" + str(self.GeneralID) + "___" + str(self.URL))

      
def ShowAttributes_of_all_LinkData(DataList):

    for link in DataList:
        link.ShowAttributes()
        
#  1. Load all Artikel List Data --------------------------------------------------------------
   

def BuildURL(SiteURL,year,month):
    if month < 10:
        month = "0" + str(month)
    return SiteURL + str(year) + str(month)

def GetAllArticlesFromPage(URL):
     req = requests.get(URL)
     soup = BeautifulSoup(req.content,'html.parser')
     Text = soup.find("h1").text
     Text = Text.strip("\n\r")
     print("URL:" + URL +" Date:" + Text )
     Articles = []
     for link in soup.find_all("h3",class_="text1"):
         ArticleURL = link.select('a[href]')[0]['href']
         Articles.append(ArticleURL)
     return Articles

def ScrapeFromGolem():
    LinkDataList = []
    century = 1998#1998
    year = 98 #98
    month = 1 #1
    TicketURL = "https://www.golem.de/aa-" #9801
    ArticleCount = 1 #1
    
    if year <= 9:
        year = "0" + str(year)
    
    while century <= 2019:
        while month <= 12:
            URL = BuildURL(TicketURL,year,month)
            ArticlesFromMonth = GetAllArticlesFromPage(URL)
            
            MonthArticleID = 1
            for Article in ArticlesFromMonth:

                link = LinkData(month,century, MonthArticleID, ArticleCount, Article)
                
                LinkDataList.append(link)
                ArticleCount = ArticleCount + 1
                MonthArticleID = MonthArticleID + 1
            
            print("Articles in " + str(month) + "/" + str(year) + ": " + str(len(ArticlesFromMonth)))
            month= month + 1
            ArticlesFromMonth.clear()
        month = 1
        year = int(year) + 1
        century = century + 1
        
        if year == 100:
            year = 0
            century = "2000"
            century = int(century)
        if year <= 9:
            year = "0" + str(year)
        else:
            year = str(year)
    return LinkDataList

#  2. Mine Artikel HTMl Data to Disk --------------------------------------------------------------------------   
  
def GetFullHTMLPage(URL):
    req = requests.get(URL)
    soup = BeautifulSoup(req.content,'html.parser')
    return soup.prettify(formatter="html")

def ConvertPrettyToBeauty(PrettySoup):
    return BeautifulSoup(PrettySoup,'html.parser')
    
    #Example Data Name: 2006_7_611_45285___45285
def WriteSingleArticleToDisk(ArticleLink):
    
        if os.path.isdir('Golem_Articles') == False:
            os.mkdir('Golem_Articles')
        
        global counter
        
        DataName = str(ArticleLink.Year) + "_" +  str(ArticleLink.Month) + "_" + str(ArticleLink.MonthID) + "_" + str(ArticleLink.GeneralID) + "___" + str(counter)

        
        Dir = "Golem_Articles/"+ DataName + ".html"
        
        HtmlPage = GetFullHTMLPage(ArticleLink.URL)
        
        with open( Dir, "w",encoding="utf-8") as file:
            #SplittedHTML = HtmlPage.split("<div class=\"g g6 activity\"")[1]
            file.write(HtmlPage)
        counter = counter + 1
        
def WriteArtikelListToDisk(ArticleLinkList):
    
    for ArticleLink in ArticleLinkList:
        WriteSingleArticleToDisk(ArticleLink)
        
# 3. Load the HTML Data from Disk to Cache ------------------------------------------------------------------

# 3.1  List all Files from the Directory
def GetArticleLinkListFromDisk(PathOfScript):
    PathOfScript = "I:\Repositorys\GolemDataMining\it-newspage-article-data-mining"
    os.chdir(PathOfScript)
    os.getcwd()
    os.chdir("./Golem_Articles")
    print("current dir is: " + os.getcwd())
    Linklist = os.listdir()
    return Linklist 

# 3.2 Sort this List naturally
    
def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

# 3.3 Read Article with LinkList from Zip
    
def ReadArticleHTML(Articlename,Path):
    Dir = Path + "\\"+ str(Articlename)
    #print("TheDir is: " + Dir)
    with open( Dir, "r",encoding="utf-8") as file:
        data= file.read()
        
    #print(Articlename + " Loaded");
    return data


def ReadFromZip(liste,PathOfArticles):
    
    HTML_List = []
    
    os.chdir(PathOfArticles)
    
    zf = zipfile.ZipFile('AllArticles.zip')
    for filename in liste:
        try:
            data = zf.read(filename)
        except KeyError:
            print ('ERROR: Did not find ' + filename + ' in zip file', )
        else:
            print (filename +  ':')
            HTML_List.append(data)
            #print (data)
    return HTML_List

#Read From Path is possible, but very slow
def LoadDataFromLinkList(Linklist,Path):
    
    HTML_List = []
    
    for Link in Linklist:
        ArticleHtml = ReadArticleHTML(Link,Path)
        HTML_List.append(ArticleHtml)
    
    return HTML_List   




# 4. Extract the data from the articles to DataObjects ------------------------------------------------------

def FormatDate(DateString):
    
    try:
        #Input Example: 2003-03-14T10:15:00+01:00 
        # First 10 Chars: 2003-03-14
        if(DateString == None or DateString == ""):
            return ""
        
        FormatedDate = DateString[0:19]
        FormatedDate = FormatedDate.replace("T","-")
        #02.01.1900 00:00:00
        FormatedDate = datetime.strptime(FormatedDate, '%Y-%m-%d-%H:%M:%S').strftime('%d.%m.%y %H:%M:%S')
        
        return FormatedDate

    except:
        print ('Problem with DateString')
        return ""


def GetDateOfPublish(soup):
    
    try:
        pattern = re.compile('"datePublished":(.*)')
        DateString =  re.search(pattern,soup.text)[0]
        DateString = re.sub('"datePublished":',"",DateString)
        DateString = re.sub(",","",DateString)
        DateString = re.sub("","",DateString)
        DateString = re.sub("\"","",DateString)
        DateString = re.sub("(?m)^\s+", "", DateString)
        return DateString
    except:
        print ('ERROR: Did not find PublishDate in HTML', )
        return ""
    
def GetDateOfModification(soup):
    try:
        pattern = re.compile('"dateModified":(.*)')
        DateString =  re.search(pattern,soup.text)[0]
        DateString = re.sub('"dateModified":',"",DateString)
        DateString = re.sub(",","",DateString)
        DateString = re.sub("","",DateString)
        DateString = re.sub("\"","",DateString)
        DateString = re.sub("(?m)^\s+", "", DateString)
        return DateString
    except:
        print ('ERROR: Did not find ModificationDate in HTML', )
        return ""

# Bug: Headline is always None
def GetHeadline(soup):
    
    try:
        headline = soup.find('h1').text
        
        if(headline == None):
            return ""
        
        headline = re.sub(r'[\ \n]{2,}', '', headline)
        return headline
    except:
        print ('ERROR: Did not find Headline in HTML', )
        return ""

def GetKeywords(soup):
    
    try:
        Keywords = soup.find('meta', attrs={'name': 'news_keywords'})
        
        if(Keywords == None):
            return ""
        
        Keywords = Keywords.attrs["content"]
        return Keywords
    except:
        print ('ERROR: Did not find Keyword in HTML', )
        return ""

def GetAuthor(soup):
    try:
        Author = soup.find(rel="author")
        
        if(Author == None):
            return ""
        
        Author = Author.text
        
        Author = re.sub(r'[\ \n]{2,}', '', Author)
        return Author
    except:
        print ('ERROR: Did not find Author in HTML', )
        return ""

def GetDescription(soup):
    try:
        desc = soup.find("header", class_="cluster-header")
        
        text = desc.p.text
        
        if(desc == None):
            return ""
        
        formatedtext = re.sub(r'[\ \n]{2,}', ' ', text)
        return formatedtext
    except:
        print ('ERROR: Did not find Description in HTML', )
        return ""

def GetOverline(soup):
    try:
        Overline = soup.find('h1').span.text
        
        if(Overline == None):
            return ""
        
        Overline = re.sub(r'[\ \n]{2,}', '', Overline)
        if Overline != "":
            Overline = re.sub('\r','',Overline)
        
        return Overline
    except:
        print ('ERROR: Did not find Overline in HTML', )
        return ""


def GetArticleTexts(soup):
    try:
        parts = 1
        AllParts = ""
        while(parts < 15):
            section = soup.find("p", { "id" : "gpar" + str(parts)})
        
            if section == None:
                break;
                
            section = re.sub(r'[\ \n]{2,}',' ', section.text)
            AllParts = AllParts + section
            parts = parts + 1
            
        if AllParts != "":
            AllParts = re.sub('\r','',AllParts)
        return AllParts

    except Exception as e:
        print ('ERROR: Did not find ArticleText in HTML', )
        print (e)
        return ""


def MineAttributesAndCreateArticleDataObjectFromSoup(soup):
    
    Article = ArticleData()
    
    Article.PublishDate = FormatDate(GetDateOfPublish(soup))
    Article.ModifyDate = FormatDate(GetDateOfModification(soup))
    Article.Headline = GetHeadline(soup)
    Article.Keywords = GetKeywords(soup)
    Article.Author = GetAuthor(soup)
    Article.Description = GetDescription(soup)
    Article.Text = GetArticleTexts(soup)
    Article.Overline = GetOverline(soup)
    
    return Article

def CreateArticleDataObjectsFromHTML_List(Articles):
    
    ArticleDataObjectList = []
    
    for Article in Articles:
        
        #soup =  BeautifulSoup(Article,'html.parser')
        soup = ConvertPrettyToBeauty(Article)
        ArticleDataObject = MineAttributesAndCreateArticleDataObjectFromSoup(soup)
        
        ArticleDataObject.ShowAttributes()
        
        if ArticleDataObject != None :
            ArticleDataObjectList.append(ArticleDataObject)
            
    return ArticleDataObjectList


# 5 Save the variables of the DataObjects to a excel File ---------------------------------------------------


def WriteArticleDataListToExcelSheet(ArticleDataList):
    
    wb = Workbook()  # could this be a problem?
    
    ws = wb.active
    PositionInSheet = 2

    ws.cell(row=1, column=1, value ='ArticleID')    
    ws.cell(row=1, column=2, value ='Headline')    
    ws.cell(row=1, column=3, value ='PublishDate')
    ws.cell(row=1, column=4, value ='ModifyDate')
    ws.cell(row=1, column=5, value ='Keywords')
    ws.cell(row=1, column=6, value ='Author')
    ws.cell(row=1, column=7, value ='Description')
    ws.cell(row=1, column=8, value ='Overline')
    ws.cell(row=1, column=9, value ='Text')
    
    
    for ArticleData in ArticleDataList:
        ws.cell(row=PositionInSheet, column=1, value=str(PositionInSheet-1))
        ws.cell(row=PositionInSheet, column=2, value=str(ArticleData.Headline))
        ws.cell(row=PositionInSheet, column=3, value=str(ArticleData.PublishDate))
        ws.cell(row=PositionInSheet, column=4, value=str(ArticleData.ModifyDate))
        ws.cell(row=PositionInSheet, column=5, value=str(ArticleData.Keywords))
        ws.cell(row=PositionInSheet, column=6, value=str(ArticleData.Author))
        ws.cell(row=PositionInSheet, column=7, value=str(ArticleData.Description))
        ws.cell(row=PositionInSheet, column=8, value=str(ArticleData.Overline))
        try:
            ws.cell(row=PositionInSheet, column=9, value=str(ArticleData.Text).encode("ascii",errors="ignore"))
        except Exception as e:
            print (e)
            ws.cell(row=PositionInSheet, column=9, value=str("").encode("ascii",errors="ignore"))
        finally:
            PositionInSheet = PositionInSheet + 1

    
    wb.save('../ArticleData.xlsx')
